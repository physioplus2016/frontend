(function () {
    'use strict';

    /**
     * @ngdoc overview
     * @name frontendApp
     * @description
     * # frontendApp
     *
     * Main module of the application.
     */
    angular
        .module('frontendApp', [
            'ngRoute',
            'nvd3',
            'pascalprecht.translate'
        ])
        .constant('Leap', window.Leap)
        .config(function ($routeProvider, $translateProvider, $provide) {
            $routeProvider
                .when('/', {
                    templateUrl: 'views/login.html',
                    controller: 'LoginCtrl',
                    controllerAs: 'loginVm'
                })
                .when('/doctor/:doctorId', {
                    templateUrl: 'views/doctor.html',
                    controller: 'DoctorCtrl',
                    controllerAs: 'doctorVm'
                })
                .when('/patient/:patientId', {
                    templateUrl: 'views/patient.html',
                    controller: 'PatientCtrl',
                    controllerAs: 'patientVm'
                })
                .otherwise({
                    redirectTo: '/'
                });
            $translateProvider.translations('en', {
                'holdTime': 'Hold Time',
                'completionRate': 'Completion Rate',
                'progressIndex': 'Progress Index',
                'exertionAngle': 'Exertion Angle',
                'painIndex': 'Pain Index'
            });
            $translateProvider.preferredLanguage('en');
        });
})();
