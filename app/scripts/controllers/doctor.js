(function () {
    'use strict';

    /**
     * @ngdoc function
     * @name frontendApp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of the frontendApp
     */
    angular.module('frontendApp')
        .controller('DoctorCtrl', ['repository', '$routeParams', 'DoctorModel', function (repository, $routeParams, DoctorModel) {
            var vm = this;
            vm.model = new DoctorModel();
            vm.model.newPatient = {};

            vm.model.doctorId = $routeParams.doctorId;
            repository.getPatients(vm.model.doctorId).then(function (res) {
                vm.model.patients = res.data;
            });
            vm.addPatient = addPatient;
            vm.updateExercise = updateExercise;
            vm.setPatient = setPatient;

            function addPatient() {
                repository.createPatient(vm.model.doctorId, {
                    name: vm.model.newPatient.name,
                    phone: vm.model.newPatient.phone,
                    email: vm.model.newPatient.email,
                    notes: vm.model.newPatient.notes
                }).then(function () {
                    repository.getPatients(vm.model.doctorId).then(function (res) {
                        vm.model.patients = res.data;
                    });
                });
                vm.model.newPatient = {};
            }

            function updateExercise() {
                repository.updateExercise(vm.model.doctorId, vm.model.patient.patientId, vm.model.exercise);
            }

            function setPatient(patient) {
                vm.model.patient = patient;
                vm.model.exercise = null;
                vm.model.detail = null;
                repository.getExercises(vm.model.doctorId, vm.model.patient.patientId).then(function (res) {
                    vm.model.patient.exercises = res.data;
                });
            }
        }]);
})();