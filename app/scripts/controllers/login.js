(function () {
  'use strict';

  /**
   * @ngdoc function
   * @name frontendApp.controller:MainCtrl
   * @description
   * # MainCtrl
   * Controller of the frontendApp
   */
  angular.module('frontendApp')
    .controller('LoginCtrl', ['$location', function ($location) {
        var vm = this;

        vm.login = function () {
            if (vm.username == 'patient') {
                $location.path( '/patient/1' );
            } else {
                $location.path( '/doctor/1' );
            }
        }
    }]);
})();
