(function () {
    'use strict';

    /**
     * @ngdoc function
     * @name frontendApp.controller:MainCtrl
     * @description
     * # MainCtrl
     * Controller of the frontendApp
     */
    angular.module('frontendApp')
        .controller('PatientCtrl', function (repository, PatientModel, $routeParams) {
            var vm = this;
            vm.model = new PatientModel();
            vm.model.patientId = $routeParams.patientId;
            vm.model.doctorId = 1;
            vm.model.patient = {
                patientId: vm.model.patientId
            };
            vm.startDailyExercise = startDailyExercise;

            repository.getExercises(vm.model.doctorId, vm.model.patientId).then(function(res) {
                vm.model.patient.exercises = res.data;
            });

            function startDailyExercise() {
                vm.model.dailyExercise = {};
            }

        });
})();