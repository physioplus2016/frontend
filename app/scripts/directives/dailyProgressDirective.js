(function () {
    'use strict';

    angular.module('frontendApp')
        .directive('dailyProgress', function (repository) {
            return {
                restrict: 'E',
                templateUrl: '../../views/dailyProgressDirective.html',
                scope: {
                    model: '='
                },
                controller: controller,
                bindToController: {
                    model: '='
                },
                controllerAs: 'dailyProgressVm'
            };

            function controller($scope) {
                var vm = this;
                vm.model = $scope.model;
                vm.setDailyProgressDetail = setDailyProgressDetail;
                vm.updateDailyProgress = updateDailyProgress;

                function setDailyProgressDetail(detail) {
                    vm.model.detail = detail;
                }

                function updateDailyProgress() {
                    repository.updateDailyProgress(vm.model.doctorId, vm.model.patientId,
                        vm.model.exercise.exerciseId, vm.model.detail);
                }
            }
        });
})();
