(function () {
    'use strict';

    angular.module('frontendApp')
        .directive('exercise', function (RunExercise, repository) {
            return {
                restrict: 'E',
                templateUrl: '../../views/exerciseDirective.html',
                scope: {
                    model: '='
                },
                controller: controller,
                bindToController: {
                    model: '='
                },
                controllerAs: 'exerciseVm'
            };

            function controller($scope) {
                var vm = this;
                vm.model = $scope.model;

                vm.model.totalSets = 2;
                vm.range = new Array(vm.model.totalSets);
                vm.model.runExercise = new RunExercise(vm.model.totalSets, 3, $scope);
                var exerciseNumber;
                switch(vm.model.exercise.name) {
                    case ('Exercise 1'):
                        exerciseNumber = 0;
                        break;
                    case ('Exercise 2'):
                        exerciseNumber = 1;
                        break;
                    case ('Exercise 3'):
                        exerciseNumber = 2;
                        break;
                    default:
                        exerciseNumber = 0;
                }
                vm.model.runExercise.doExercise(exerciseNumber)
                    .then(function (dailyProgress) {
                        var day = vm.model.exercise.dailyProgress.length + 1;
                        dailyProgress.progressIndex = calculateProgressIndex(day,
                            dailyProgress.completionRate, dailyProgress.holdTime, dailyProgress.exertionAngle,
                            day > 1 ? vm.model.exercise.dailyProgress[day - 2].progressIndex : null);
                        vm.model.progressIndex = dailyProgress.progressIndex;
                        repository.setDailyProgress(vm.model.doctorId, vm.model.patientId, vm.model.exercise.exerciseId, dailyProgress);
                        vm.model.dailyExercise = null;
                        repository.getDailyProgress(vm.model.doctorId, vm.model.patientId, vm.model.exercise.exerciseId).then(function (res) {
                            vm.model.exercise.dailyProgress = res.data;
                        })
                    });

                function calculateProgressIndex(day, completionRate, holdTime, exertionAngle, previousProgress) {
                    var progressIndex = (completionRate + holdTime + exertionAngle) / 3;
                    if (day != 1 && !angular.isUndefined(previousProgress)) {
                        progressIndex = (previousProgress + progressIndex) / 2;
                    }
                    return progressIndex;
                }
            }
        });
})();