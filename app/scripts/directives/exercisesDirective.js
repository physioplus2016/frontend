(function () {
    'use strict';

    angular.module('frontendApp')
        .directive('exercises', function (repository) {
            return {
                restrict: 'E',
                templateUrl: '../../views/exercisesDirective.html',
                scope: {
                   model: '=',
                    isDoctor: '='
                },
                controller: controller,
                bindToController: {
                    model: '=',
                    isDoctor: '='
                },
                controllerAs: 'exercisesVm'
            };

            function controller($scope) {
                var vm = this;
                vm.model = $scope.model;
                vm.isDoctor = $scope.isDoctor;

                vm.addExercise = addExercise;
                vm.setExercise = setExercise;

                function addExercise() {
                    repository.assignExercise(vm.model.doctorId, vm.model.patient.patientId, {
                        name: vm.model.newExercise.name,
                        notes: vm.model.newExercise.notes
                    }).then(function () {
                        repository.getExercises(vm.model.doctorId, vm.model.patient.patientId).then(function (res) {
                            vm.model.patient.exercises = res.data;
                        });
                    });
                    vm.model.newExercise = {};
                }

                function setExercise(exercise) {
                    vm.model.exercise = exercise;
                    vm.model.detail = null;
                    vm.model.dailyExercise = null;
                    vm.model.runExercise = null;
                    vm.model.progressIndex = null;
                    repository.getDailyProgress(vm.model.doctorId, vm.model.patient.patientId, exercise.exerciseId).then(function (res) {
                        vm.model.exercise.dailyProgress = res.data;
                        vm.model.exercise.data = [{
                            key: 'Progress',
                            values: vm.model.exercise.dailyProgress
                        }];
                    });
                    vm.model.exercise.options = {
                        chart: {
                            type: 'lineChart',
                            height: 250,
                            width: 450,
                            showLegend: false,
                            margin: {
                                top: 20,
                                right: 30,
                                bottom: 40,
                                left: 60
                            },
                            x: function (d) {
                                return new Date(d.date);
                            },
                            y: function (d) {
                                return d.progressIndex;
                            },
                            useInteractiveGuideline: true,
                            xAxis: {
                                axisLabel: 'Date',
                                tickFormat: function (d) {
                                    return d3.time.format('%x')(new Date(d));
                                }
                            },
                            yAxis: {
                                axisLabel: 'Progress (%)',
                                tickFormat: function (d) {
                                    return d3.format('%')(d);
                                }
                            },
                            yDomain: [0, 1]

                        }
                    };
                }
            }
        });
})();