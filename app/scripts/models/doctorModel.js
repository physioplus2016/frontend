(function () {
    'use strict';

    angular.module('frontendApp')
        .service('DoctorModel', function () {
            return function () {
                return {
                    type: 'doctor'
                };
            };
        });
})();