(function () {
    'use strict';

    angular.module('frontendApp')
        .service('PatientModel', function () {
            return function () {
                return {
                    type: 'patient'
                };
            };
        });
})();