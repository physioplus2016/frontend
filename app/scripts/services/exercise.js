(function () {
    'use strict';

    angular.module('frontendApp')
        .service('RunExercise', function ($timeout, $q) {
                var model = this;
                return function (totalSets, totalNumberInSet, scope) {
                    model.start = 0;
                    model.currentSet = 1;
                    model.breakCounter = 0;
                    model.completedSets = [];
                    model.totalSets = totalSets;
                    model.totalNumberInSet = totalNumberInSet;
                    model.scope = scope;

                    model.doExercise = doExercise;
                    model.getBreakCounter = getBreakCounter;
                    model.isStarted = isStarted;
                    model.getCurrentSet = getCurrentSet;
                    model.getCompletedSets = getCompletedSets;
                    return model;
                };

                function getBreakCounter() {
                    return model.breakCounter;
                }

                function isStarted() {
                    return model.start;
                }

                function getCurrentSet() {
                    return model.currentSet;
                }

                function getCompletedSets() {
                    return model.completedSets;
                }

                function doExercise(exerciseIndex) {
                    return $q(function (resolve) {
                        var data = {
                            completionRate: 0,
                            holdTime: 0,
                            exertionAngle: 0
                        };

                        var chain = $q.when();
                        for (var i = 0; i < model.totalSets; i++) {
                            var count = 0;
                            for (var j = 0; j < model.totalNumberInSet; j++) {
                                chain = chain.then(function () {
                                    return doExerciseSet(exerciseIndex).then(function (response) {
                                        model.start = 0;
                                        data.completionRate += response.completionRate;
                                        data.holdTime += response.holdTime;
                                        data.exertionAngle += response.exertionAngle;
                                        count += 1;
                                        if (model.totalNumberInSet === count) {
                                            model.completedSets.push({
                                                success: response.completionRate
                                            });
                                            model.currentSet += 1;
                                            var pauseTime = 3000;
                                            model.breakCounter = pauseTime;
                                            var interval = setInterval(function () {
                                                model.breakCounter -= 1000;
                                            }, 1000);
                                            $timeout(function () {
                                                model.breakCounter = 0;
                                                clearInterval(interval);
                                            }, pauseTime);
                                        }
                                    });
                                });
                            }
                        }

                        chain.then(function () {
                            var iterations = model.totalNumberInSet * model.totalSets;
                            data.completionRate /= iterations;
                            data.holdTime /= iterations;
                            data.exertionAngle /= iterations;
                            resolve(data);
                        });
                    });
                }

                function doExerciseSet(exerciseIndex) {
                    return $q(function (resolve) {

                        var controller = new Leap.Controller({loopWhileDisconnected: false});
                        var time = 0;
                        var completion = 0;
                        var effort = 0;
                        var frameId = 0;
                        var frameRef;
                        var movementDone = false;
                        var startFrame;
                        var startTime = 0;
                        var rotTrack = 0;
                        var rotNew = 1;
                        var grabTrack = 0;
                        var grabNew = 1;
                        var grabRef;
                        switch (exerciseIndex) {
                            case (0):
                                controller.loop(function (frame) {
                                    if (frame.id < frameId + 30) {
                                        return;
                                    }
                                    frameId = frame.id;

                                    // if (frame.hands.length > 0)
                                    //     console.log(frame.hands[0].direction);
                                    if (startFrame == undefined &&
                                        frame.hands.length > 0 &&
                                        frame.hands[0].direction[0] < 0 && frame.hands[0].direction[0] > -0.3 &&
                                        Math.abs(frame.hands[0].direction[1]) < 0.3 &&
                                        Math.abs(frame.hands[0].direction[2]) > 0.8) {
                                        model.start = 1;
                                        model.scope.$digest();
                                        startFrame = frame;
                                    } else if (startFrame != undefined && !movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        rotNew = frame.hands[0].rotationAngle(startFrame, [0, -1, 0]);
                                        if (Math.abs(rotNew - rotTrack) < 0.05) {
                                            frameRef = frame;
                                            movementDone = true;
                                            startTime = frame.timestamp;
                                        } else if (rotNew > rotTrack) {
                                            rotTrack = rotNew;
                                        }
                                    } else if (movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        rotNew = frame.hands[0].rotationAngle(frameRef, [0, -1, 0]);
                                        if (!effort) {
                                            effort = frame.hands[0].rotationAngle(startFrame, [0, -1, 0]) / (Math.PI / 2);
                                        }
                                        if (frame.timestamp - startTime >= 3000000) {
                                            time = 1;
                                            completion = 1;
                                            controller.disconnect();
                                        } else if (Math.abs(rotNew) < 0.025) {
                                            time = (frame.timestamp - startTime) / 3000000;
                                            if (time < 0.5) {
                                                completion = 0;
                                            } else {
                                                completion = 1;
                                            }
                                            controller.disconnect();
                                        }
                                    }
                                });
                                break;
                            case (1):
                                controller.loop(function (frame) {
                                    if (frame.id < frameId + 30) {
                                        return;
                                    }
                                    frameId = frame.id;

                                    if (frame.hands.length > 0)
                                        console.log(frame.hands[0].direction);
                                    if (startFrame == undefined &&
                                        frame.hands.length > 0 &&
                                        frame.hands[0].direction[0] > 0 && frame.hands[0].direction[0] < 0.3 &&
                                        Math.abs(frame.hands[0].direction[1]) < 0.3 &&
                                        Math.abs(frame.hands[0].direction[2]) > 0.8) {
                                        model.start = 1;
                                        model.scope.$digest();
                                        startFrame = frame;
                                    } else if (startFrame != undefined && !movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        rotNew = frame.rotationAngle(startFrame, [0, -1, 0]);
                                        if (Math.abs(rotNew - rotTrack) < 0.05) {
                                            frameRef = frame;
                                            movementDone = true;
                                            startTime = frame.timestamp;
                                        } else if (rotNew < rotTrack) {
                                            rotTrack = rotNew;
                                        }
                                    } else if (movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        rotNew = frame.rotationAngle(frameRef, [0, -1, 0]);
                                        if (!effort) {
                                            effort = Math.abs(frame.rotationAngle(startFrame, [0, -1, 0])) / (Math.PI / 2);
                                        }
                                        if (frame.timestamp - startTime >= 3000000) {
                                            time = 1;
                                            completion = 1;
                                            controller.disconnect();
                                        } else if (Math.abs(rotNew) < 0.025) {
                                            time = (frame.timestamp - startTime) / 3000000;
                                            if (time < 0.5) {
                                                completion = 0;
                                            } else {
                                                completion = 1;
                                            }
                                            controller.disconnect();
                                        }
                                    }
                                });
                                break;
                            case (2):
                                controller.loop(function (frame) {
                                    if (frame.id < frameId + 30) {
                                        return;
                                    }
                                    frameId = frame.id;

                                    // if (frame.hands.length > 0)
                                    //     console.log(frame.hands[0].grabStrength);
                                    if (startFrame == undefined &&
                                        frame.hands.length > 0 &&
                                        frame.hands[0].grabStrength < 0.01) {
                                        model.start = 1;
                                        model.scope.$digest();
                                        startFrame = frame;
                                    } else if (startFrame != undefined && !movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        grabNew = frame.hands[0].grabStrength;
                                        if (Math.abs(grabNew - grabTrack) < 0.01 && grabNew > 0.01) {
                                            grabRef = frame.hands[0].grabStrength;
                                            movementDone = true;
                                            startTime = frame.timestamp;
                                        } else if (grabNew > grabTrack) {
                                            grabTrack = grabNew;
                                        }
                                    } else if (movementDone) {
                                        if (frame.hands.length == 0) {
                                            startFrame = undefined;
                                            model.start = 0;
                                            model.scope.$digest();
                                            return;
                                        }
                                        grabNew = frame.hands[0].grabStrength - grabRef;
                                        if (!effort) {
                                            effort = grabRef;
                                        }
                                        if (frame.timestamp - startTime > 3000000) {
                                            time = 1;
                                            completion = 1;
                                            controller.disconnect();
                                        } else if (Math.abs(grabNew) < 0.001) {
                                            time = (frame.timestamp - startTime) / 3000000;
                                            if (time < 0.5) {
                                                completion = 0;
                                            } else {
                                                completion = 1;
                                            }
                                            controller.disconnect();
                                        }
                                    }
                                });
                        }

                        controller.connect();
                        controller.on('disconnect', function () {
                            return resolve({
                                completionRate: completion,
                                holdTime: time,
                                exertionAngle: effort
                            });
                        });


                    });
                }
            }
        );
})();

