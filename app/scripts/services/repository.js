(function () {
    'use strict';

    angular.module('frontendApp')
        .service('repository', ['$http', function ($http) {
            var baseUrl = 'http://54.173.47.247:8080/physio/api';
            // var baseUrl = 'http://localhost:8080/physio/api';
            return {
                getPatients: getPatients,
                createPatient: createPatient,
                getExercises: getExercises,
                assignExercise: assignExercise,
                updateExercise: updateExercise,
                getDailyProgress: getDailyProgress,
                setDailyProgress: setDailyProgress,
                updateDailyProgress: updateDailyProgress
            };


            function getPatients(doctorId) {
                return $http.get(baseUrl + '/doctors/' + doctorId + '/patients');
            }

            function createPatient(doctorId, patient) {
                return $http.post(baseUrl + '/doctors/' + doctorId + '/patients', patient);
            }

            function getExercises(doctorId, patientId) {
                return $http.get(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises');
            }

            function assignExercise(doctorId, patientId, exercise) {
                return $http.post(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises', exercise);
            }

            function updateExercise(doctorId, patientId, exercise) {
                return $http.put(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises/' + exercise.exerciseId, exercise);
            }

            function getDailyProgress(doctorId, patientId, exerciseId) {
                return $http.get(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises/' + exerciseId + '/daily-progress');
            }

            function updateDailyProgress(doctorId, patientId, exerciseId, dailyProgress) {
                return $http.put(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises/' + exerciseId + '/daily-progress/' + dailyProgress.dailyProgressId, dailyProgress);
            }

            function setDailyProgress(doctorId, patientId, exerciseId, dailyProgress) {
                return $http.post(baseUrl + '/doctors/' + doctorId + '/patients/' + patientId + '/exercises/' + exerciseId + '/daily-progress', dailyProgress);
            }
        }]);
})();